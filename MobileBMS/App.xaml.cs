﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MobileBMS
{
    public partial class App : Application
    {
        public TcpClient tcpC = new TcpClient(); // Global TcpClient object
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());
        }
        public string message;
        public string GetMessage()
        {
            return message;
        }
        protected async override void OnStart()
        {
            await ToggleConnection();
        }
        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
        public async Task ToggleConnection()
        {
            if (canToglleIt)
            {
                performReadDataFromPlcConstans = !performReadDataFromPlcConstans;
                canToglleIt = false;
                if (performReadDataFromPlcConstans)
                {
                    canToglleIt = true;
                    tcpC = new TcpClient();
                    byte[] byAdres = new byte[4];
                    // set IP address of PLC
                    byAdres[0] = 192; byAdres[1] = 168; byAdres[2] = 3; byAdres[3] = 250;
                    IPAddress ipAdress = new IPAddress(byAdres);
                    bool isSelfPass = StartConnection(ipAdress);
                    if (tcpC.Connected && isSelfPass)
                    {
                        await CyclicJob(500);
                    }
                    else
                    {
                        performReadDataFromPlcConstans = false;
                    }
                }
                else
                {
                    await CycleStop();
                }
            }
        }
        #region Function for perform Ping test with real PLC
        private bool MakePingTest(IPAddress IPAddressForTest)
        {
            bool pingAns = false;
            Ping pingSender = new Ping();
            PingReply reply = pingSender.Send(IPAddressForTest);
            if (reply.Status == IPStatus.Success)
            {
                pingAns = true;
            }
            return pingAns;
        }
        #endregion

        #region Part of code for generci TcpClinet perform connection for more info please check C# documnetation
        private void ConnectTCP(IPAddress IPAddressToConnect, int portNumber)
        {
            tcpC.ReceiveTimeout = 5;
            tcpC.SendTimeout = 5;
            try
            {
                tcpC = Connect(IPAddressToConnect, portNumber, 1000);
            }
            catch
            {
                message += "Port Open FAIL\n";
            }
        }
        private TcpClient Connect(IPAddress hostName, int port, int timeout)
        {
            var client = new TcpClient();
            var state = new State { Client = client, Success = true };
            IAsyncResult ar = client.BeginConnect(hostName, port, EndConnect, state);
            state.Success = ar.AsyncWaitHandle.WaitOne(timeout, false);
            if (!state.Success || !client.Connected)
                throw new Exception("Failed to connect.");
            return client;
        }
        private class State
        {
            public TcpClient Client { get; set; }
            public bool Success { get; set; }
        }
        private void EndConnect(IAsyncResult ar)
        {
            var state = (State)ar.AsyncState;
            TcpClient client = state.Client;
            try
            {
                client.EndConnect(ar);
            }
            catch { }
            if (client.Connected && state.Success)
                return;
            client.Close();
        }
        #endregion

        #region Part of code used to verify whether the communication function operates normally or not
        private bool SelfTest()
        {
            bool loopTestAns = false;

            byte[] loopMessage = new byte[5] { 0x41, 0x42, 0x43, 0x44, 0x45 }; // 5 elements for test - "ABCDE"


            //Request data length
            int needByteMessage = 2 + 4 + 2 + loopMessage.Length;
            byte lowByte = (byte)(needByteMessage & 0xff);
            byte highByte = (byte)(needByteMessage >> 8 & 0xff);


            byte[] payload = new byte[] { 0x50, 0x00, 0x00, 0xff, 0xff, 0x03, 0x00, lowByte, highByte, 0x10, 0x00,
                                          0x19, 0x06,0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };


            //number of loopack data
            lowByte = (byte)(loopMessage.Length & 0xff);
            highByte = (byte)(loopMessage.Length >> 8 & 0xff);
            payload[15] = lowByte; payload[16] = highByte;

            // loopack data 
            for (int i = 0; i < loopMessage.Length; i++)
            {
                payload[17 + i] = loopMessage[i];
            }

            NetworkStream stream = tcpC.GetStream();
            stream.Write(payload, 0, payload.Length);
            byte[] data = new Byte[20];
            stream.ReadTimeout = 1000;
            try
            {
                Int32 bytes = stream.Read(data, 0, data.Length);
                if (data[9] == 0 && data[10] == 0 && data[11] == lowByte && data[12] == highByte)
                {
                    loopTestAns = true;
                    for (int i = 0; i < loopMessage.Length; i++)
                    {
                        if (loopMessage[i] != data[13 + i])
                        {
                            loopTestAns = false;
                        }
                    }
                }
            }
            catch
            {
                loopTestAns = false;
            }
            return loopTestAns;
        }
        #endregion
        int numberOfReadedByte;
        int bytesIndex = 0;
        
        private void ReadFromD50ToD81()
        {
            byte[] payload = new byte[] { 0x50, 0x00, 0x00, 0xff, 0xff, 0x03, 0x00, 0x0C, 0x00, 0x10, 0x00, 0x01, 0x04, 0x00, 0x00
                , 0x32, 0x00, 0x00, 0xA8, 0x20, 0x00 };
            NetworkStream stream = tcpC.GetStream();
            stream.Write(payload, 0, payload.Length);
            byte[] data = new Byte[200];
            stream.ReadTimeout = 1000;
            try
            {
                numberOfReadedByte = stream.Read(data, 0, data.Length);
                if (data[9] == 0 && data[10] == 0)
                {
                    bytesIndex = 11;
                    for (int i = 0; i < 16; i++)
                    {
                        slmpData.minTempVal[i] = BitConverter.ToInt16(data, bytesIndex);
                        slmpData.maxTempVal[i] = BitConverter.ToInt16(data, bytesIndex + 2);
                        bytesIndex += 4;
                    }
                }
                else
                {
                    message += "Error in Answer\n";
                }
            }
            catch
            {
                message += "Error in interpreter\n";
            }
        }
        private void WriteFromD50ToD81()
        {
            byte[] startPayload = new byte[] { 0x50, 0x00, 0x00, 0xff, 0xff, 0x03, 0x00, 0x4C, 0x00, 0x10, 0x00, 0x01, 0x14, 0x00, 0x00
                , 0x32, 0x00, 0x00, 0xA8, 0x20, 0x00 };
            byte[] payload = new byte[21+64];
            for (int i = 0; i < startPayload.Length; i++)
            {
                payload[i] = startPayload[i];
            }
            int arrIndex = 21;
            byte[] afterConv;
            for (int i = 0; i < 16; i++)
            {
                afterConv = BitConverter.GetBytes(slmpData.minTempVal[i]);
                payload[arrIndex] = afterConv[0]; payload[arrIndex + 1] = afterConv[1];
                arrIndex += 2;
                afterConv = BitConverter.GetBytes(slmpData.maxTempVal[i]);
                payload[arrIndex] = afterConv[0]; payload[arrIndex + 1] = afterConv[1];
                arrIndex += 2;
            }
            NetworkStream stream = tcpC.GetStream();
            stream.Write(payload, 0, payload.Length);
            byte[] data = new Byte[20];
            stream.ReadTimeout = 1000;
            try
            {
                numberOfReadedByte = stream.Read(data, 0, data.Length);
                if (data[9] == 0 && data[10] == 0)
                {
                    message += "OK write from D100 to D131";
                }
                else
                {
                    message += "Error in Answer\n";
                }
            }
            catch
            {
                message += "Error in interpreter\n";
            }
        }
        private void ReadFromD82ToD153()
        {
            byte[] payload = new byte[] { 0x50, 0x00, 0x00, 0xff, 0xff, 0x03, 0x00, 0x0C, 0x00, 0x10, 0x00, 0x01, 0x04, 0x00, 0x00
                , 0x52, 0x00, 0x00, 0xA8, 0x48, 0x00 };
            NetworkStream stream = tcpC.GetStream();
            stream.Write(payload, 0, payload.Length);
            byte[] data = new Byte[300];
            stream.ReadTimeout = 1000;
            try
            {
                numberOfReadedByte = stream.Read(data, 0, data.Length);
                if (data[9] == 0 && data[10] == 0)
                {
                    bytesIndex = 11;
                    for (int i = 0; i < 16; i++)
                    {
                        slmpData.currentTemp[i] = BitConverter.ToSingle(data, bytesIndex);
                        bytesIndex += 4;
                    }
                    for (int i = 0; i < 16; i++)
                    {
                        slmpData.roomsPowerUsage[i] = BitConverter.ToSingle(data, bytesIndex);
                        bytesIndex += 4;
                    }
                    for (int i = 0; i < 3; i++)
                    {
                        slmpData.floorsPowerUsage[i] = BitConverter.ToSingle(data, bytesIndex);
                        bytesIndex += 4;
                    }
                    slmpData.totalPowerUsage = BitConverter.ToSingle(data, bytesIndex);
                }
                else
                {
                    message += "Error in Answer\n";
                }
            }
            catch
            {
                message += "Error in interpreter\n";
            }
        }
        private void ReadFromM100ToM115()
        {
            byte[] payload = new byte[] { 0x50, 0x00, 0x00, 0xff, 0xff, 0x03, 0x00, 0x0C, 0x00, 0x10, 0x00, 0x01, 0x04, 0x00, 0x00
                , 0x64, 0x00, 0x00, 0x90, 0x01, 0x00 };
            NetworkStream stream = tcpC.GetStream();
            stream.Write(payload, 0, payload.Length);
            byte[] data = new Byte[200];
            stream.ReadTimeout = 1000;
            try
            {
                numberOfReadedByte = stream.Read(data, 0, data.Length);
                if (data[9] == 0 && data[10] == 0)
                {
                    bytesIndex = 11;
                    byte[] dataToConv = new byte[] { data[11], data[12] };
                    slmpData.fbInWorkAirCon = new BitArray(dataToConv);
                }
                else
                {
                    message += "Error in Answer\n";
                }
            }
            catch
            {
                message += "Error in interpreter\n";
            }
        }
        private void WriteFromM100ToM115()
        {
            int[] array = new int[1];
            slmpData.fbInWorkAirCon.CopyTo(array, 0);
            byte[] afterConvert = BitConverter.GetBytes(array[0]);
            byte[] payload = new byte[] { 0x50, 0x00, 0x00, 0xff, 0xff, 0x03, 0x00, 0x0E, 0x00, 0x10, 0x00, 0x01, 0x14, 0x00, 0x00
                , 0x64, 0x00, 0x00, 0x90, 0x01, 0x00, afterConvert[0], afterConvert[1] };
            NetworkStream stream = tcpC.GetStream();
            stream.Write(payload, 0, payload.Length);
            byte[] data = new Byte[20];
            stream.ReadTimeout = 1000;
            try
            {
                numberOfReadedByte = stream.Read(data, 0, data.Length);
                if (data[9] == 0 && data[10] == 0)
                {
                    message += "\nOK write from M100 to M115";
                }
                else
                {
                    message += "Error in Answer\n";
                }
            }
            catch
            {
                message += "Error in interpreter\n";
            }
        }
        private void ReadFromM116ToM179()
        {
            byte[] payload = new byte[] { 0x50, 0x00, 0x00, 0xff, 0xff, 0x03, 0x00, 0x0C, 0x00, 0x10, 0x00, 0x01, 0x04, 0x00, 0x00
                , 0x74, 0x00, 0x00, 0x90, 0x04, 0x00 };
            NetworkStream stream = tcpC.GetStream();
            stream.Write(payload, 0, payload.Length);
            byte[] data = new Byte[200];
            stream.ReadTimeout = 1000;
            try
            {
                numberOfReadedByte = stream.Read(data, 0, data.Length);
                if (data[9] == 0 && data[10] == 0)
                {
                    bytesIndex = 11;
                    byte[] dataToConv = new byte[8];
                    for (int i = 0; i < 8; i++)
                    {
                        dataToConv[i] = data[11 + i];
                    }
                    BitArray tempBitArray = new BitArray(dataToConv);
                    for (int i = 0; i < 16; i++)
                    {
                        slmpData.fbOutIsWorking[i] = tempBitArray[4 * i + 0];
                        slmpData.fbOutIsHeating[i] = tempBitArray[4 * i + 1];
                        slmpData.fbOutIsCooling[i] = tempBitArray[4 * i + 2];
                        slmpData.fbOutErrorSetTemp[i] = tempBitArray[4 * i + 3];
                    }
                }
                else
                {
                    message += "Error in Answer\n";
                }
            }
            catch
            {
                message += "Error in interpreter\n";
            }
        }
        bool performReadDataFromPlcConstans = false;
        bool canToglleIt = true;
        int numberOfPerform = 0;
        private async void Button_Clicked2(object sender, EventArgs e)
        {
            await ToggleConnection();
        }
        private bool StartConnection(IPAddress ipAdress)
        {
            bool isSlefPass = false;
            if (true)//MakePingTest(ipAdress)) // check if you can find divice with set address
            {
                ConnectTCP(ipAdress, 2000); //connection for set IP address and Port No.
                if (tcpC.Connected) // check availability for SLMP connection
                {
                    if (SelfTest())// verify communication function
                    {
                        message += "All tests PASS\n";
                        isSlefPass = true;
                    }
                    else
                    {
                        message += "Binary set FAIL\n";
                    }
                }
                else
                {
                    message += "No available SLMP connection\n";
                }
            }
            else
            {
                message += "Ping Test FAIL\n";
            }
            return isSlefPass;
        }
        public class SlmpDataConverted
        {
            public BitArray fbInWorkAirCon = new BitArray(16);
            public bool[] fbOutIsWorking = new bool[16];
            public bool[] fbOutIsHeating = new bool[16];
            public bool[] fbOutIsCooling = new bool[16];
            public bool[] fbOutErrorSetTemp= new bool[16];
            public short[] minTempVal = new short[16];
            public short[] maxTempVal = new short[16];
            public float[] currentTemp = new float[16];
            public float[] roomsPowerUsage = new float[16];
            public float[] floorsPowerUsage = new float[3];
            public float totalPowerUsage;
        }
        public SlmpDataConverted slmpData = new SlmpDataConverted();
        bool cycleJobState = false;
        private async Task CyclicJob(int intervalMs)
        {
            AsyncFunctionInitial();
            cycleJobState = true;
            while (performReadDataFromPlcConstans)
            {
                message = string.Empty;
                AsyncFunctionInScan();
                await Task.Delay(intervalMs);
            }
            cycleJobState = false;
        }
        private async Task CycleStop()
        {
            int numberOfLoops = 0;
            while (cycleJobState)
            {
                numberOfLoops++;
                await Task.Delay(500);
            }
            Application.Current.Dispatcher.BeginInvokeOnMainThread(() => {
                message = "Finish communication, number of loops: " + numberOfLoops.ToString();
            });
            canToglleIt = true;
            tcpC.Close();
            tcpC.Dispose();
        }
        private void AsyncFunctionInitial()
        {
            ReadFromM100ToM115();
            ReadFromD50ToD81();
            numberOfPerform = 1;
        }
        private void AsyncFunctionInScan()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            WriteFromD50ToD81();
            WriteFromM100ToM115();
            ReadFromD82ToD153();            
            ReadFromM116ToM179();
            numberOfPerform++;
            sw.Stop();
            message += "\nPerformed times: " + numberOfPerform.ToString() + "\nTime to read: " + sw.ElapsedMilliseconds.ToString();
        }
    }
}