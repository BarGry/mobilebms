﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileBMS
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PageLevel2 : ContentPage
    {
        App currApp;
        public PageLevel2()
        {
            InitializeComponent();
            currApp = (App)Application.Current;
            StartAsyncTask();
        }
        public async Task StartAsyncTask()
        {
            while (true)
            {
                Application.Current.Dispatcher.BeginInvokeOnMainThread(() =>
                {
                    entryPowerUsage.Text = currApp.slmpData.floorsPowerUsage[2].ToString("0.000") + " kW";
                    buttonRoom21.Text = "Pokój 21\n" + currApp.slmpData.currentTemp[10].ToString("0.0") + "°C";
                    buttonRoom22.Text = "Pokój 22\n" + currApp.slmpData.currentTemp[11].ToString("0.0") + "°C";
                    buttonRoom23.Text = "Pokój 23\n" + currApp.slmpData.currentTemp[12].ToString("0.0") + "°C";
                    buttonRoom24.Text = "Pokój 24\n" + currApp.slmpData.currentTemp[13].ToString("0.0") + "°C";
                    buttonRoom25.Text = "Pokój 25\n" + currApp.slmpData.currentTemp[14].ToString("0.0") + "°C";
                    buttonRoom26.Text = "Pokój 26\n" + currApp.slmpData.currentTemp[15].ToString("0.0") + "°C";
                });
                await Task.Delay(200);
            }
        }
        private async void buttonRoom21_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RoomCommon(10, "21"));
        }
        private async void buttonRoom22_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RoomCommon(11, "22"));
        }
        private async void buttonRoom23_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RoomCommon(12, "23"));
        }
        private async void buttonRoom24_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RoomCommon(13, "24"));
        }
        private async void buttonRoom25_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RoomCommon(14, "25"));
        }
        private async void buttonRoom26_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RoomCommon(15, "26"));
        }
    }
}