﻿using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileBMS
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RoomCommon : ContentPage
    {
        App currApp;
        byte currRoomIndex = 0;
        string currVisableName = string.Empty;
        public RoomCommon(byte roomIndex, string visableName)
        {

            InitializeComponent();
            Title = "Pokój " + visableName;
            currApp = (App)Application.Current;
            currRoomIndex = roomIndex;
            currVisableName = visableName;
            entryMinTemp.Text = currApp.slmpData.minTempVal[currRoomIndex].ToString();
            entryMaxTemp.Text = currApp.slmpData.maxTempVal[currRoomIndex].ToString();
            entryCurrentTemp.Text = currApp.slmpData.currentTemp[currRoomIndex].ToString("0.0") + "°C";
            StartAsyncTask();
        }
        SolidColorBrush solidColorDisable = new SolidColorBrush(Color.White);
        SolidColorBrush solidColorWorking = new SolidColorBrush(Color.Green);
        SolidColorBrush solidColorError = new SolidColorBrush(Color.Red);
        SolidColorBrush solidColorChill = new SolidColorBrush(Color.Blue);
        SolidColorBrush solidColorHeating = new SolidColorBrush(Color.Yellow);
        public async Task StartAsyncTask()
        {
            while (true)
            {
                Application.Current.Dispatcher.BeginInvokeOnMainThread(() => { 
                    //labelRoomInfo.Text = MakeTextInfoAboutCurrentRoom();
                    entryCurrentTemp.Text = currApp.slmpData.currentTemp[currRoomIndex].ToString("0.0") + "°C";
                    entryPowerRoom.Text = currApp.slmpData.roomsPowerUsage[currRoomIndex].ToString("0.000") + " kW";

                    if (currApp.slmpData.fbOutIsWorking[currRoomIndex])
                        ellipseWorking.Fill = solidColorWorking;
                    else
                        ellipseWorking.Fill = solidColorDisable;

                    if (currApp.slmpData.fbOutErrorSetTemp[currRoomIndex])
                        ellipseError.Fill = solidColorError;
                    else
                        ellipseError.Fill = solidColorDisable;

                    if (currApp.slmpData.fbOutIsCooling[currRoomIndex])
                        ellipseChill.Fill = solidColorChill;
                    else
                        ellipseChill.Fill = solidColorDisable;

                    if (currApp.slmpData.fbOutIsHeating[currRoomIndex])
                        ellipseHeating.Fill = solidColorHeating;
                    else
                        ellipseHeating.Fill = solidColorDisable;
                });
                await Task.Delay(200);
            }
        }
        //public string MakeTextInfoAboutCurrentRoom()
        //{
        //    return "Wszystkie dane [Pokój " + currVisableName +
        //        "]\nObecna temperatura: " + currApp.slmpData.currentTemp[currRoomIndex].ToString("0.0") +
        //        "\nMinimana temperatura: " + currApp.slmpData.minTempVal[currRoomIndex].ToString() +
        //        "\nMaksymalna temperatura: " + currApp.slmpData.maxTempVal[currRoomIndex].ToString() +
        //        "\nZałączenie bloku funkcyjnego: " + currApp.slmpData.fbInWorkAirCon[currRoomIndex].ToString() +
        //        "\nBlok funkcyjny działa: " + currApp.slmpData.fbOutIsWorking[currRoomIndex].ToString() +
        //        "\nZałączenie ogrzewania: " + currApp.slmpData.fbOutIsHeating[currRoomIndex].ToString() +
        //        "\nZałączenie chłodzenia: " + currApp.slmpData.fbOutIsCooling[currRoomIndex].ToString() +
        //        "\nBłąd ustawienia temperatury: " + currApp.slmpData.fbOutErrorSetTemp[currRoomIndex].ToString() +
        //        "\nZapotrzbowanie mocy dla pokoju: " + currApp.slmpData.roomsPowerUsage[currRoomIndex].ToString("0.000");
        //}
        private void buttonToggleFb_Clicked(object sender, System.EventArgs e)
        {
            currApp.slmpData.fbInWorkAirCon[currRoomIndex] = !currApp.slmpData.fbInWorkAirCon[currRoomIndex];
        }
        private void entryMinTemp_Completed(object sender, System.EventArgs e)
        {
            try
            {
                currApp.slmpData.minTempVal[currRoomIndex] = short.Parse(entryMinTemp.Text);
            }
            catch (System.Exception)
            {
                entryMinTemp.Text = currApp.slmpData.minTempVal[currRoomIndex].ToString();
            }
        }
        private void entryMaxTemp_Completed(object sender, System.EventArgs e)
        {
            try
            {
                currApp.slmpData.maxTempVal[currRoomIndex] = short.Parse(entryMaxTemp.Text);
            }
            catch (System.Exception)
            {
                entryMaxTemp.Text = currApp.slmpData.maxTempVal[currRoomIndex].ToString();
            }
        }
        private void buttonMinTempAdd_Clicked(object sender, System.EventArgs e)
        {
            currApp.slmpData.minTempVal[currRoomIndex] += 1;
            entryMinTemp.Text = currApp.slmpData.minTempVal[currRoomIndex].ToString();
        }
        private void buttonMinTempSub_Clicked(object sender, System.EventArgs e)
        {
            currApp.slmpData.minTempVal[currRoomIndex] -= 1;
            entryMinTemp.Text = currApp.slmpData.minTempVal[currRoomIndex].ToString();
        }
        private void buttonMaxTempAdd_Clicked(object sender, System.EventArgs e)
        {
            currApp.slmpData.maxTempVal[currRoomIndex] += 1;
            entryMaxTemp.Text = currApp.slmpData.maxTempVal[currRoomIndex].ToString();
        }
        private void buttonMaxTempSub_Clicked(object sender, System.EventArgs e)
        {
            currApp.slmpData.maxTempVal[currRoomIndex] -= 1;
            entryMaxTemp.Text = currApp.slmpData.maxTempVal[currRoomIndex].ToString();
        }
    }
}